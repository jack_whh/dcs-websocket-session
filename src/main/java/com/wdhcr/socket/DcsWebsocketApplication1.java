package com.wdhcr.socket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DcsWebsocketApplication1 {

    public static void main(String[] args) {
        SpringApplication.run(DcsWebsocketApplication1.class, args);
    }

}
