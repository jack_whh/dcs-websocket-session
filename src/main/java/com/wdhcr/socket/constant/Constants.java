package com.wdhcr.socket.constant;

/**
 * @description: 常量类
 * @dateTime: 2021/6/17 16:21
 */
public class Constants {
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /** redis 订阅消息通道标识*/
    public final static String REDIS_CHANNEL = "shoNewestMsg";

    /** 消息体的key*/
    public final static String REDIS_MESSAGE_KEY = "KEY";
    /** 消息体的值*/
    public final static String REDIS_MESSAGE_VALUE = "VALUE";
}
